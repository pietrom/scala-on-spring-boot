package org.amicofragile.learning.sosb.clock

import java.time.ZonedDateTime

trait Clock {
  def now : ZonedDateTime
}
