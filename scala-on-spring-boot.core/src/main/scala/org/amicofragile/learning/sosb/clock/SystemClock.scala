package org.amicofragile.learning.sosb.clock

import java.time.ZonedDateTime

class SystemClock extends Clock {
  def now() = ZonedDateTime.now()
}
