package org.amicofragile.learning.sosb.clock

import java.time.ZonedDateTime

class FixedClock (val now : ZonedDateTime) extends Clock
