package org.amicofragile.learning.sosb.web

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.ComponentScan

object WebApp extends App {
  SpringApplication.run(classOf[WebApp])
}

@SpringBootApplication
@ComponentScan(basePackages = Array("org.amicofragile.learning.sosb.web"))
class WebApp extends SpringBootServletInitializer {
  protected override def configure(application: SpringApplicationBuilder): SpringApplicationBuilder =
    application.sources(classOf[WebApp])
}
