package org.amicofragile.learning.sosb.web

import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.amicofragile.learning.sosb.clock.{Clock, SystemClock}
import org.springframework.context.annotation.{Bean, Configuration, PropertySource}
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@PropertySource(Array("classpath:/app.properties"))
class WebConfiguration extends WebMvcConfigurer {
  @Bean
  def clock: Clock = new SystemClock()

  @Bean
  def objectMapper : ObjectMapper = new ScalaObjectMapper()
}

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
class ScalaObjectMapper extends ObjectMapper  {
  {
    registerModule(DefaultScalaModule)
    registerModule(new JavaTimeModule())
    disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
  }
}
