package org.amicofragile.learning.sosb.web.healthcheck

import java.time.ZonedDateTime

import org.amicofragile.learning.sosb.clock.Clock
import org.springframework.web.bind.annotation.{RequestMapping, RestController}

@RestController
@RequestMapping(Array("/api/health-check"))
class HealthCheckController(private val clock: Clock) {
  @RequestMapping(Array(""))
  def check = HealthCheckResult.ok(clock.now)
}

case class HealthCheckResult(occurredOn: ZonedDateTime, status: Boolean)

object HealthCheckResult {
  def ok(occurredOn: ZonedDateTime) = HealthCheckResult(occurredOn, true)
}
